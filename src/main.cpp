#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <ESP8266WebServer.h>
#include "html.h" // acá está declarada lcdpage

LiquidCrystal_I2C lcd{0x3F, 16, 2};
ESP8266WebServer server{80};

void escribir() {
  // Si los campos no están ambos vacíos, se escribe su contenido
  if(not (server.arg("l0").isEmpty()) or not (server.arg("l1").isEmpty())) {
    lcd.clear(); // Se borra el display y se posiciona en (0,0)
    lcd.print(server.arg("l0")); // texto del input con name='l0'
    lcd.setCursor(0,1);
    lcd.print(server.arg("l1")); // texto del input con name='l1'
  }
  server.send(200,"text/html",lcdpage); // se envia el html
}

void setup() {
  // Inicialización del Display
  lcd.init();
  lcd.backlight();

  // Inicialización del WiFi en modo AP
  WiFi.mode(WIFI_AP);
  while(!WiFi.softAP("El LCD Wi-Fi")) {
    lcd.print('.');
    delay(100);
  }

  // Se muestra la IP local del ESP8266
  lcd.setCursor(0, 0);
  lcd.print("IP:");
  lcd.print(WiFi.softAPIP());
  
  // Se configura el WebServer para atender peticiones en '/' 
  //  y '/escribir' (misma respuesta)
  server.on("/",escribir);
  server.on("/escribir",escribir);
  server.onNotFound([]{server.send(404,"text/plain","404 - Pagina no encontrada.");});
  server.begin();

  // Se muestra el mensaje de que el sevidor está funcionado
  lcd.setCursor(0,1);
  lcd.print(" Server Iniciado");
}

void loop() {
  // Atendiendo a las peticiones
  server.handleClient();
}