# WebServer para escribir en un LCD con un ESP8266

Este es un ejemplo sencillo para escribir en un display de 16x2 a base de carcteres a través de una página web embebida y servida por un ESP8266 utilizando la plataforma Arduino.

Para más información accedé a [mi blog](https://blog.tute-avalos.com/2022/08/26/paginas-embebidas-webserver-esp8266/)